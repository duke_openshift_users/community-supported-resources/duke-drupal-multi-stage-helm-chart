if ! oc get secret {{ deploy_key.name }} 
  then
    oc create secret generic {{ deploy_key.name }} \
    --from-file=ssh-privatekey={{ deploy_key.local_path }} \
    --type=kubernetes.io/ssh-auth

    oc annotate secret {{ deploy_key.name }} 'build.openshift.io/source-secret-match-uri-1=ssh://gitlab.oit.duke.edu/*'

    oc secrets link builder {{ deploy_key.name }}

    oc patch secret {{ deploy_key.name }} -p '{"metadata":{"labels":{"app":"{{ okd_project_name }}"}}}'
  fi