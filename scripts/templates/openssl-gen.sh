openssl genrsa -out {{ cert.common_name }}.key 4096
openssl req -config openssl.conf -new -nodes -key {{ cert.common_name }}.key -out {{ cert.common_name }}.csr
chmod 0400 {{ cert.common_name }}.key
openssl req -config openssl.conf -days 3650 -new -nodes -key {{ cert.common_name }}.key -out {{ cert.common_name }}.crt -x509
openssl req -in {{ cert.common_name }}.csr -text