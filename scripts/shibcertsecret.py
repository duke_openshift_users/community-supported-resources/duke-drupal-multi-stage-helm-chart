import os
import sys
import logging
import jinja2
import yaml
import rstr
import subprocess

BASE_DIR= os.path.realpath(os.path.join(os.path.dirname(__file__)))

logger = logging.getLogger('main')
logger.addHandler(logging.StreamHandler(sys.stdout))
logger.setLevel('INFO')

class Main(object):

    @staticmethod
    def _get_output_dir():
        path = os.path.join(BASE_DIR, 'output')
        if not os.path.exists(path):
            os.makedirs(path)
        return path

    @staticmethod
    def _load_config():
        config_file = os.path.join( BASE_DIR, os.path.pardir, 'values.yaml')
        with open(config_file) as stream:
            config = yaml.load(stream, Loader=yaml.CSafeLoader)
        
        return config

    def run(self):
        context = self._load_config()

        env = jinja2.Environment(loader=jinja2.FileSystemLoader(os.path.join(BASE_DIR, 'templates')))

        context.update(dict(
            key_file=os.path.join(BASE_DIR, 'output', '%s.key' % context['okd_project_name'] + '.cloud.duke.edu'),
            crt_file=os.path.join(BASE_DIR, 'output', '%s.crt' % context['okd_project_name'] + '.cloud.duke.edu'),
            csr_file=os.path.join(BASE_DIR, 'output', '%s.csr' % context['okd_project_name'] + '.cloud.duke.edu'),
            openssl_conf_file=os.path.join(BASE_DIR, 'templates', 'openssl.conf')
        ))

        env = jinja2.Environment(loader=jinja2.FileSystemLoader(os.path.join(BASE_DIR, 'templates')))
        for file in os.listdir(os.path.join(BASE_DIR, 'templates')):

            if (file != '.gitkeep'):

                template = env.get_template(file)
                with open(os.path.join(BASE_DIR, 'output', file), 'wt') as stream:
                    context[file.replace('-', '_')] = template.render(context)
                    stream.write(context[file.replace('-', '_')])

        subprocess.call([
            '/bin/sh',
            'openssl-gen.sh'
            ], cwd=os.path.join(BASE_DIR, 'output')
        )

        subprocess.call([
            '/bin/sh',
            'share-image-from-project.sh'
        ], cwd=os.path.join(BASE_DIR, 'output'))

        subprocess.call([
            '/bin/sh',
            'oc-create-shib-cert-secret.sh'
        ], cwd=os.path.join(BASE_DIR, 'output'))

        subprocess.call([
            '/bin/sh',
            'create-deploy-key-secret.sh'
        ], cwd=os.path.join(BASE_DIR, 'output'))

if __name__ == '__main__':
    Main().run()
