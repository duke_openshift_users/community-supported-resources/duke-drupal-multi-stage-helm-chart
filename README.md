# how to use

this chart requires helm 3 to be installed locally, along with the openshift cli

```
brew install helm
```

also

``` 
brew install openshift-cli
```

then log into openshift through the command line

make sure your oc client is on the correct project.

```
oc project PROJECTNAME
```

cd into the `scripts` directory and run

```
python3 shibcertsecret.py
```

This will generate a self-signed cert and create the appropriate openshift objects.

This also allows your current project to pull images from the `dws` namespace.

once that's done, cd back to the root directory and run:

```
helm install NAME .
```

where NAME is anything you want. Typically, you can just call it bb8, but it can be a project name (lowercase and hyphens only)


Please note, this is the very first release of this chart. It implies the use of a custom S2I builder image which currently lives in the `dws` namespace. It also leverages a shibboleth image in the `dws` namespace, but that should change in the relatively near future. If you want to test this out, you will need to be added to the `dws` namespace to grab those images. Ping me and I can add you. 

